<?php

$params = require __DIR__ . '/params.php';
// Next line is Yii2 v2.0.15.1 default code
// $db = require __DIR__ . '/db.php';

// This block added by dbyrd
if ( file_exists(__DIR__ . '/db-local.php') !== false ){
    // db-local.php will array merge itself with db.php
    $db = require __DIR__ . '/db-local.php';
} else {
    $db = require __DIR__ . '/db.php';
}

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '7SdDkayjtV-qdzloa4PovxTvxfynLCbl',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        /* default Yii User Class is a Component. The dektrium User Class is a Module
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        */
        'errorHandler' => [
            'errorAction' => 'site/error',
            // web error handler
            'class' => '\bedezign\yii2\audit\components\web\ErrorHandler',   
            // console error handler
            //'class' => '\bedezign\yii2\audit\components\console\ErrorHandler',            
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'db' => $db,
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => true,
            'rules' => [
            ],
        ],
        
        'authManager' => [
            //'class' => 'yii\rbac\DbManager',
            'class' => 'dektrium\rbac\components\DbManager'
        ],
        
    ],
    'params' => $params,
    'modules' => [
        /* dektrium */
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['admin','dana-byrd',]
        ],
        'rbac' => 'dektrium\rbac\RbacWebModule',
        'audit' => 'bedezign\yii2\audit\Audit',
    ],    
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
