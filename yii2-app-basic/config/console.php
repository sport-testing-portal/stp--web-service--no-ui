<?php

$params = require __DIR__ . '/params.php';
// Next line is Yii2 v2.0.15.1 default code
// $db = require __DIR__ . '/db.php';

// This block added by dbyrd
if ( file_exists(__DIR__ . '/db-local.php') !== false ){
    // db-local.php will array merge itself with db.php
    $db = require __DIR__ . '/db-local.php';
} else {
    $db = require __DIR__ . '/db.php';
}


$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'], 
                    //'levels' => ['error', 'warning','trace','info'], 
                ],
            ],
        ],
        'db' => $db,
        'authManager' => [
            //'class' => 'yii\rbac\DbManager',
            'class' => 'dektrium\rbac\components\DbManager'
        ],
        'errorHandler' => [
            // console error handler
            'class' => '\bedezign\yii2\audit\components\console\ErrorHandler',          
        ],
    ],
    'modules'=>[
        'rbac' => 'dektrium\rbac\RbacConsoleModule',
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
    
     
  
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
